README file for EECS 481 hw 2
-----------------------------
The code for the simple application I developed is all in the FirstApp folder.

My application has 4 buttons. The first changes the displayed text to my name.
The second changes the displayed text to "Hello World!". The third changes the
displayed text to "jk I lied" after being pressed 5 times. The fourth displays
an image of President Obama accompanied by some facetious political commentary.

Below are two imgur links to the screenshots requested in the assignment prompt.

"hello world" style application: http://imgur.com/w3mw3mX
Added buttons that change the UI in some way: http://imgur.com/5pkic6T