package com.example.firstapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
//    public void hello(View view) {
//        Intent intent = new Intent(this, DisplayMessageActivity.class);
////        EditText editText = (EditText) findViewById(R.id.edit_message);
//        String message = "Hello World!"
//        intent.putExtra(EXTRA_MESSAGE, message);
//        startActivity(intent);
//    }
    public void hello(View view)
    {
    	TextView tv = (TextView)findViewById(R.id.textView1);
    	tv.setText("Hello World!");
    }
    
    public void name(View view)
    {
    	TextView tv = (TextView)findViewById(R.id.textView1);
    	tv.setText("Duncan Smith");
    }
    
    int store = 0;
    public void nothing(View view)
    {
    	if (store < 5)
    	{
    		store++;
    	}
    	else
    	{
    		TextView tv = (TextView)findViewById(R.id.textView1);
    		tv.setText("jk I lied");
    		store = 0;
    	}
    }
    
    boolean is = false;
    public void Obama(View view)
    {
    	if (is == false)
    	{
    		ImageView o = new ImageView(this);
    		o.setImageResource(R.drawable.obama);
    		RelativeLayout rl = (RelativeLayout) findViewById(R.id.RelativeLayout01);
    		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
    		    RelativeLayout.LayoutParams.WRAP_CONTENT,
    		    RelativeLayout.LayoutParams.WRAP_CONTENT);
    		//lp.addRule(RelativeLayout.BELOW, R.id.ButtonRecalculate);
    		//lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
    		rl.addView(o, lp);
    		is = true;
    	}
    	else
    	{
    		RelativeLayout r1 = (RelativeLayout) findViewById(R.id.RelativeLayout01);
    		r1.removeAllViews();
    		is = false;
    	}
    }
    
    
}
